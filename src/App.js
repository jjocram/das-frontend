import {useEffect, useState} from 'react';
import './App.css';
import addressSharingABI from './contracts/AddressSharing.json';
import sspermissionABI from './contracts/SSPermission.json'
import {ethers} from 'ethers';

import SecretForm from './components/SecretForm';
import RequestAccessComponent from "./components/RequestAccessComponent";
import DecryptComponent from "./components/DecryptComponent";

const addressSharingAddress = process.env.REACT_APP_ADDRESS_SHARING_ADDRESS //"0x3BBe9F9F9Ff851576c2f7CE9e286c7BAAa68c994";
const sspermissionAddress = process.env.REACT_APP_SSPERMISSION_ADDRESS //"0x7dC19C6F7621b639a69Ca11EAf64E670D1095229";

console.log("Address Sharing address: " + addressSharingAddress);
console.log("SSPermission address: " + sspermissionAddress);

const DATA_CONSUMER_ADDRESS = "0x7EEF23a0C38F394c55c55431e0D7755badD2Cd41";


function randomBytes32(length = 64) {
    let chars = "12345678890abcdef";
    let str = '0x';
    for (let i = 0; i < length; i++) {
        str += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return str;
}

const user_id = randomBytes32();


function App() {
    const [currentAccount, setCurrentAccount] = useState(null);

    const checkWalletIsConnected = async () => {
        const {ethereum} = window;

        if (!ethereum) {
            console.log("Make sure you have Metamask installed!");
            return;
        } else {
            console.log("Wallet exists! We're ready to go!")
        }

        const accounts = await ethereum.request({method: 'eth_accounts'});

        if (accounts.length !== 0) {
            const account = accounts[0];
            console.log("Found an authorized account: ", account);
            setCurrentAccount(account);
        } else {
            console.log("No authorized account found");
        }
    }

    const connectWalletHandler = async () => {
        const {ethereum} = window;

        if (!ethereum) {
            alert("Please install Metamask!");
        }

        try {
            const accounts = await ethereum.request({method: 'eth_requestAccounts'});
            console.log("Found an account! Address: ", accounts[0]);
            setCurrentAccount(accounts[0]);
        } catch (err) {
            console.log(err)
        }
    }

    const createUserHandler = async () => {
        try {
            const {ethereum} = window;

            if (ethereum) {
                const provider = new ethers.providers.Web3Provider(ethereum);
                const signer = provider.getSigner();
                const addressSharingContract = new ethers.Contract(addressSharingAddress, addressSharingABI, signer);

                console.log("Initialize user creation");
                let createUserTxn = await addressSharingContract.set_user(user_id);

                console.log("Mining... please wait");
                await createUserTxn.wait();

                console.log("User added in the blockchain");
            }
        } catch (err) {
            console.log(err)
        }
    }

    const grantAccess = async () => {
        try {
            const {ethereum} = window;

            if (ethereum) {
                const provider = new ethers.providers.Web3Provider(ethereum);
                const signer = provider.getSigner();
                const sspermissionContract = new ethers.Contract(sspermissionAddress, sspermissionABI, signer);

                console.log("Allowing access at " + [DATA_CONSUMER_ADDRESS] + " for " + user_id);
                let allowAccessTxn = await sspermissionContract.allow_access(user_id, [DATA_CONSUMER_ADDRESS]);

                console.log("Mining... please wait");
                await allowAccessTxn.wait();

                console.log("User allowed");
            }
        } catch (err) {
            console.log(err)
        }
    }

    const connectWalletButton = () => {
        return (
            <button onClick={connectWalletHandler} className='cta-button connect-wallet-button'>
                Connect Wallet
            </button>
        )
    }

    const createUserButton = () => {
        return (
            <button onClick={createUserHandler} className='cta-button interact-with-blockchain'>
                Create user
            </button>
        )
    }


    useEffect(() => {
        checkWalletIsConnected();
    }, [])

    return (
        <div className='main-app'>
            <h1>Decentralized Authorization Service dApp</h1>

            <hr/>

            <div>
                <h2 align="left">User creation</h2>
                <p>Let's create a new user in the blockchain:</p>
                {currentAccount ? createUserButton() : connectWalletButton()}
            </div>

            <hr/>

            <SecretForm userId={user_id}/>

            <hr/>

            <div>
                <h2 align="left">Grant access to the Data Consumer</h2>
                <p>Data Owner gives access to the Data Consumer</p>
                <p>Data Owner's ID: <span className="address">{user_id}</span></p>
                <p>Data Consumer's address: <span className="address">{DATA_CONSUMER_ADDRESS}</span></p>
                <button onClick={grantAccess} className='cta-button interact-with-blockchain'>
                    Grant access
                </button>
            </div>

            <hr/>

            <RequestAccessComponent dataConsumerAddress={DATA_CONSUMER_ADDRESS} userId={user_id}/>

            <hr/>

            <RequestAccessComponent dataConsumerAddress="0xAAAA23a0C38F394c55c55431e0D7755badD2Cd41" userId={user_id}/>

            <hr/>

            <DecryptComponent/>
        </div>
    );
}

export default App;
