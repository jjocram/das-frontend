import React from "react";
import CryptoJS from "crypto-js";

class SecretForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            message: "",
            privateKey: "",
            ciphertext: "",
            userId: props.userId
        }

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        console.log(e)
    }

    handleSubmit(e) {
        e.preventDefault();

        const {secretToEncrypt, privateKey} = document.forms[0];

        const encryptedMessage = CryptoJS.AES.encrypt(secretToEncrypt.value, privateKey.value)

        console.log("Ciphertext: " + encryptedMessage + " pk: " + privateKey.value);


        const requestOptions = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            withCredentials: true,
            body: JSON.stringify({
                data_owner_id: this.state.userId,
                encrypted_password: privateKey.value
            })
        };
        fetch('http://localhost:3001/provide', requestOptions)
            .then(response => response.json())
            .then(_ => {})

        this.setState(state => ({
            message: "",
            privateKey: "",
            ciphertext: encryptedMessage.toString()
        }))
    }

    render() {
        return (
            <div>
                <h2 align="left">Secret creation and encryption</h2>
                <p>The Data Owner has a secret to encrypt with a private key</p>
                <p>Data owner ID: <span className="address">{this.state.userId}</span></p>
                <div className="form">
                    <form onSubmit={this.handleSubmit}>
                        <div className="field">
                            <label htmlFor="secretToEnc">Secret to encrypt</label>
                            <input type="text" name="secretToEncrypt" id="message" required/>
                        </div>

                        <br/>

                        <div className="field">
                            <label htmlFor="privKey">Private key</label>
                            <input type="text" name="privateKey" id="privateKey" required/>
                        </div>

                        <br/>

                        <input type="submit" className="cta-button interact-with-server" value="Create secret"/>
                    </form>
                </div>

                <p>Encrypted data: {this.state.ciphertext}</p>
            </div>
        )
    }
}

export default SecretForm;
