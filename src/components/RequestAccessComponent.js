import React from "react";

class RequestAccessComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            dataConsumerAddress: props.dataConsumerAddress,
            userId: props.userId,
            allowed: "NOT REQUESTED YET",
            password: null
        }

        this.handleClick = () => {
            const requestOptions = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',

                },
                withCredentials: true,
                body: JSON.stringify({
                    data_consumer_address: this.state.dataConsumerAddress,
                    data_owner_id: this.state.userId
                })
            };

            fetch('http://localhost:3001/request-access', requestOptions)
                .then(response => response.json())
                .then(data => {
                    console.log("Access: " + data.allowed);
                    if (data.allowed) {
                        this.setState(s => ({
                            allowed: "YES",
                            password: data.password
                        }))
                    } else {
                        this.setState(s => ({
                            allowed: "NO",
                        }))
                    }
                });
        }
    }

    showPassword() {
        return (
            <p>Password: {this.state.password}</p>
        )
    }

    notShowPassword() {
        return (
            <p>Password: not available</p>
        )
    }

    render() {
        return (
            <div>
                <h2 align="left">Data Consumer requests access at the Authorization Service</h2>
                <p>Data Consumer's address: <span className="address">{this.state.dataConsumerAddress}</span></p>
                <button onClick={this.handleClick} className='cta-button interact-with-server'>
                    Request access
                </button>
                <p>Access: {this.state.allowed}</p>
                {this.state.password ? this.showPassword() : this.notShowPassword()}
            </div>
        )
    }
}

export default RequestAccessComponent;
