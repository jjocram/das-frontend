import React from "react";

const CryptoJS = require("crypto-js");

class DecryptComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            decryptedText: null
        }

        this.handleDecrypt = () => {
            const {ciphertext, privateKey} = document.forms[1];

            console.log(ciphertext.value)

            const message = CryptoJS.AES.decrypt(ciphertext.value, privateKey.value).toString(CryptoJS.enc.Utf8)
            console.log(message)

            this.setState(s => ({
                decryptedText: ""+message
            }))
        }
    }

    showMessage() {
        return (
            <p>The message was: {this.state.decryptedText}</p>
        )
    }

    render() {
        return (
            <div>
                <h2 align="left">Data Owner can decrypt the message</h2>
                <form>
                    <div className="field">
                        <label htmlFor="secretToEnc">Data to decrypt</label>
                        <input type="text" name="ciphertext" id="message" required/>
                    </div>

                    <br/>

                    <div className="field">
                        <label htmlFor="privateKey">Password from Authorization Service</label>
                        <input type="text" name="privateKey" id="privateKey" required/>
                    </div>

                    <br/>
                </form>

                <button onClick={this.handleDecrypt} className="cta-button interact-with-browser">Decrypt</button>

                {this.state.decryptedText ? this.showMessage() : (<p></p>)}
            </div>
        )
    }
}

export default DecryptComponent
